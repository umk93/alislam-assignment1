import { useState, useEffect } from "react";
import axios from "axios";

import { List, ListItem, ListItemText, Typography } from "@mui/material";

const ListComponent = () => {
  const [list, setList] = useState();

  const apiCall = async () => {
    const temp = await axios.get("https://alislam.cloud/apps/sample/api.json");
    if (temp) {
      setList(temp);
    }
  };
  useEffect(() => {
    apiCall();
  }, []);

  return (
    <div>
      {list ? (
        <List>
          <Typography>{list.title}</Typography>
          {list.apps.map((item, index) => (
            <ListItem key={index}>
              {item.icon && item.icon !== "" ? (
                <img src={item.icon} alt="list-icon" />
              ) : (
                <></>
              )}
              <ListItemText
                primary={item.name}
                secondary={<Typography>{item.desc}</Typography>}
              />
            </ListItem>
          ))}
        </List>
      ) : (
        <div>Cors error on server side</div>
      )}
    </div>
  );
};
export default ListComponent;
